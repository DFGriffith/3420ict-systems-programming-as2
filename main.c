//--------------------------------------------------//
//                                                  //
//    Assinment 2 - Multi-Threaded Client Server    //
//                                                  //
//            David Farrow, s2917529                //  
//      Griffith University, Gold Coast, 2015       // 
//                                                  //
//                 main.c | Client                  //
//                                                  //
//--------------------------------------------------//

#include <stdio.h>
#include <stdlib.h>

#include <signal.h> 	/* ctrl c handling */

#include <unistd.h> 	/* usleep */

#include <sys/types.h> 	/* shared memory */
#include <sys/ipc.h>
#include <sys/shm.h>

#include <sys/select.h>
#include <sys/time.h> 	/* timeval */

#include <windows.h>

#include "globals.h"
#include "server.h"




//--------------------------------------------//
//                Query Struct                //
//--------------------------------------------//
typedef struct 
{
	long query_input;
	int query_number;
	LARGE_INTEGER start_time;
}Query;



//--------------------------------------------//
//              Global variables              //
//--------------------------------------------//
//-- general
char client_input[1000]; 
int queryCount = 0;
Query query_array[10];

//-- timer
LARGE_INTEGER frequency;   // ticks per second
LARGE_INTEGER t2;     	   // ticks
double elapsedTime;        // time calculated with ticks

//-- fork
extern pid_t pid;
int status = 1;

//-- shared memory
int slot_array_size = 10;
extern int long_shared_mem_size;
extern int char_shared_mem_size;

extern int long_shmid;
extern int char_shmid;

extern unsigned long *number;
extern unsigned long *slot_array;
extern unsigned long *working_flag_array;
extern unsigned long *progress_array;

extern char *client_flag;
extern char *server_flag_array;

extern Semaphore *sem_1;
extern Semaphore *sem_array;
extern Semaphore *sem_working_array;





//--------------------------------------------//
//       Method Declarations(Prototypes)      //
//--------------------------------------------//
void run_client();
void query_input();
void query_print();
void handle_input();
void client_query(long);
void client_test_query(long);

int kbhit();

void zombieKill();
void crtl_c_handler();
void shutdown_handler();
void setup_shared_memory();
void shared_memory_cleanup();



//--------------------------------------------//
//                Main Method                 //
//--------------------------------------------//
int main(int argc, char *argv[])
{
	//-------- Initial Setup --------//
	signal(SIGINT, crtl_c_handler);			// ctl + c signal handling
    atexit(shutdown_handler);				// Set at exit function
    QueryPerformanceFrequency(&frequency); 	// Get ticks per second

    if(argc > 1) 							// check for number of threads argument
    {
        number_of_threads = atoi(argv[1]);
    }
    else
    {
    	number_of_threads = 32;
    }


    //-------- Shared Memory ---------//
    setup_shared_memory();


	//------------- Fork -------------//
    if ((pid = fork()) == -1)
    {
        perror("fork");
    }

    //------------ Server ------------//
    if(pid == 0) 
    {
    	server_init(number_of_threads);
        run_server(); 
    }

    //------------ Client ------------//
    else 
    {
    	run_client();
    }
   	
}





//--------------------------------------------//
//               Client Main Loop             //
//--------------------------------------------//
void run_client()
{
	int in_ready = 0;
	printf("\n\n\n\n\n\n");

	while(1)
	{
		if(queryCount == 0)
		{
			puts("\033[6A");
			printf("Enter query (or 'q' to quit) : ");
			scanf(" %[^\t\n]", client_input);
			
			handle_input();

			printf("\n\n\n\n\n\n");
		}
		else
		{
			in_ready = kbhit(); // Check whether stdin has input to be read (waits for 500ms) 

			//-- if input is ready to be read --//
			if(in_ready != 0) 	
			{
				query_input();	
			}
			//-- else update console output --//
			else					
			{
				query_print();
			}

		}
	}
}


void query_input()
{
	scanf(" %[^\t\n]", client_input);

	if(queryCount < 10)
	{
		handle_input();
		puts("\033[1A");
	}
	else
	{
		printf("\n\n\nSystem busy........\n\n\n");
		printf("\n\n\n\n\n\n");
	}
}


void query_print()
{
	int i = 0;
	for(;i < 10; i++)
	{ 
		if(server_flag_array[i] == '1')
		{
			int d = number_of_threads * 100;
			int p = d - progress_array[i];

			puts("\033[7A");

			printf("\n-----------------------\n");
			printf("Query %d                 \n", i);
			printf("Reply : %u               \n", slot_array[i]);
			printf("Percent : %g%%           \n", (((double)p)/d)*100 );

			if(working_flag_array[i] <= 0 )
			{
		        QueryPerformanceCounter(&t2);
		        elapsedTime = (t2.QuadPart - query_array[i].start_time.QuadPart) * 1000.0 / frequency.QuadPart;
		        
		        printf("Complete for : %u           \n", query_array[i].query_input);
		        printf("Time: %G ms                 \n", elapsedTime);
		        printf("-----------------------\n\n\n\n\n\n\n");

		        queryCount--;
			}
			else
			{
				printf("-----------------------\n");
			}
			
			server_flag_array[i] = '0';
			//break;
		}
	}
}








//--------------------------------------------//
//           Query Handling Methods           //
//--------------------------------------------//
void handle_input()
{
	long number_to_factor;

	//-- if client quit message 'q'
    if(strcmp(client_input, "q") == 0) 	
    {
        exit(0);
    }
    //-- if test query '0'
    else if(strcmp(client_input, "0") == 0)
    {
    	if (queryCount > 0)
    	{
    		puts("\033[7A");
    		printf("System busy (There must be no outstanding queries to run test)...\n\n\n\n\n\n");
    	}
    	else
    	{
    		client_test_query(0);
    	}
    }
    //-- check if inout is a number
	else if((number_to_factor = atol(client_input)) == 0)
    {
    	printf("Sorry : query must be a 32bit integer...\n");
    }
    //-- start standard query
    else
    {
    	client_query(number_to_factor);
    }
}


void client_query(long number_to_factor)
{	
	queryCount++;

    sem_wait(sem_1);

    while(*client_flag != '0')
    {
    	usleep(100);
    }

	*number = number_to_factor;
	*client_flag = '1';

	sem_signal(sem_1);

	while(*client_flag != '0')
    {
    	usleep(100);
    }
    int slot_num = *number;

    query_array[slot_num].query_number = queryCount;
    query_array[slot_num].query_input = number_to_factor;
    QueryPerformanceCounter(&query_array[slot_num].start_time);
}


void client_test_query(long number_to_factor)
{
	queryCount+=3;

	sem_wait(sem_1);

    while(*client_flag != '0')
    {
    	usleep(100);
    }

	*number = number_to_factor;
	*client_flag = '1';

	sem_signal(sem_1);

	while(*client_flag != '0')
    {
    	usleep(100);
    }
    int slot_num = *number;

    query_array[0].query_number = 1;
    query_array[0].query_input = number_to_factor;
    QueryPerformanceCounter(&query_array[0].start_time);

    query_array[1].query_number = 2;
    query_array[1].query_input = number_to_factor;
    QueryPerformanceCounter(&query_array[1].start_time);

    query_array[2].query_number = 3;
    query_array[2].query_input = number_to_factor;
    QueryPerformanceCounter(&query_array[2].start_time);
}






//--------------------------------------------//
//    Shared Memory Creation & Destruction    //
//--------------------------------------------//
void setup_shared_memory()
{
	//------- Long Shared Memory ------//
	long_shared_mem_size = sizeof(unsigned long) * (1 + (slot_array_size * 3));

    /* connect to (and possibly create) the segment: */
    if ((long_shmid = shmget(IPC_PRIVATE, long_shared_mem_size, 0xffff)) == -1) 
    {
        perror("shmget");
        exit(1);
    }
    
    /* attach to the segment to get a pointer to it: */
    number = (unsigned long *) shmat(long_shmid, (void *)0, 0);
    if (number == NULL) 
    {
        perror("shmat");
        exit(1);
    }

    slot_array = number + 1; 				// get the pointer for the slot array
    working_flag_array = slot_array + 10; 
    progress_array = working_flag_array + 10;
 

    //------- Char Shared Memory ------//
	char_shared_mem_size = sizeof(char) * (1 + slot_array_size);

    /* connect to (and possibly create) the segment: */
    if ((char_shmid = shmget(IPC_PRIVATE, char_shared_mem_size, 0xffff)) == -1) 
    {
        perror("shmget");
        exit(1);
    }
    
    /* attach to the segment to get a pointer to it: */
    client_flag = (char *) shmat(char_shmid, (void *)0, 0);
    if (client_flag == NULL) 
    {
        perror("shmat");
        exit(1);
    }
    server_flag_array = client_flag + 1;	// get the pointer for the server flag array


    //----- Semaphore Shared Memory ----//
	sem_shared_mem_size = sizeof(Semaphore) * (1 + slot_array_size);

    /* connect to (and possibly create) the segment: */
    if ((sem_shmid = shmget(IPC_PRIVATE, sem_shared_mem_size, 0xffff)) == -1) 
    {
        perror("shmget");
        exit(1);
    }
    
    /* attach to the segment to get a pointer to it: */
   	sem_1 = (Semaphore *) shmat(sem_shmid, (void *)0, 0);
    if (sem_1 == NULL) 
    {
        perror("shmat");
        exit(1);
    }
    sem_array = sem_1 + 1;	// get the pointer for the server flag array
	sem_working_array = sem_array + 10;


    //--- Set shared memory values ---//
    int i = 0;
    for (; i < slot_array_size; ++i)
    {
    	slot_array[i] = 0;
    	working_flag_array[i] = 0;

    	server_flag_array[i] = '0';

    	make_semaphore(&sem_array[i], 1);
    	make_semaphore(&sem_working_array[i], 1);
    }
    make_semaphore(sem_1, 1);
    
    *client_flag = '0';
}


void shared_memory_cleanup()
{
	//-- detach shared memory segment --//
	shmdt(number);  
	shmdt(client_flag);
	shmdt(sem_1);

	//-- remove shared memory segment --//  
	shmctl(long_shmid, IPC_RMID, NULL);
	shmctl(char_shmid, IPC_RMID, NULL);
	shmctl(sem_shmid, IPC_RMID, NULL);
}






//---------------------------------------------//
//  kbhit - Checks STDIN for n amount of time  //
//---------------------------------------------//
int kbhit()
{
    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 1000;

    fd_set fds;		// fd_set passed into select
    
    FD_ZERO(&fds);	// Zero out the fd_set
    FD_SET(STDIN_FILENO, &fds);

    select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
    
    return FD_ISSET(STDIN_FILENO, &fds);	// return 0 if STDIN is not ready to be read.
}






//--------------------------------------------//
//              Shutdown Methods              //
//--------------------------------------------//
void crtl_c_handler()
{
	exit(0);
}


void shutdown_handler() 
{
    if(pid == 0) 
    {
    	server_shutdown();
    	exit(0);	
    }

    shared_memory_cleanup();
    
    zombieKill();

    printf("\nA nice termination message...\n");
}


void zombieKill()
{
	kill(pid, SIGKILL);
}

