//--------------------------------------------------//
//                                                  //
//    Assinment 2 - Multi-Threaded Client Server    //
//                                                  //
//            David Farrow, s2917529                //  
//      Griffith University, Gold Coast, 2015       // 
//                                                  //
//             globals.h | Global Data              //
//                                                  //
//--------------------------------------------------//

#ifndef _GLOBALS_
#define _GLOBALS_

#include <sys/types.h> 	// shared memory
#include <sys/ipc.h>
#include <sys/shm.h>
#include "semaphore.h"

//--------------------------------------------//
//              Global variables              //
//--------------------------------------------//
//-- general
int number_of_threads;

//-- fork
pid_t pid;

//----- Shared Memory -----//
int long_shared_mem_size;
int char_shared_mem_size;
int sem_shared_mem_size;

//-- ids
int long_shmid;
int char_shmid;
int sem_shmid;

//-- data
unsigned long *number;
unsigned long *slot_array;
unsigned long *working_flag_array;
unsigned long *progress_array;

//-- flags
char *client_flag;
char *server_flag_array;

//-- semaphores
Semaphore *sem_1;
Semaphore *sem_array;
Semaphore *sem_working_array; 

#endif
