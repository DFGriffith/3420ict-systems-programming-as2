//--------------------------------------------------//
//                                                  //
//    Assinment 2 - Multi-Threaded Client Server    //
//                                                  //
//            David Farrow, s2917529                //  
//      Griffith University, Gold Coast, 2015       // 
//                                                  //
//                server.c | Server                 //
//                                                  //
//--------------------------------------------------//

#include "server.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>  /* For sqrt() */



//--------------------------------------------//
//              Global variables              //
//--------------------------------------------//
//-- general
int q_percent[10];
Semaphore progress_sem[10];

//-- fork
extern pid_t pid;

//-- shared memory
extern int long_shared_mem_size;
extern int char_shared_mem_size;

extern int long_shmid;
extern int char_shmid;

extern unsigned long *number;
extern unsigned long *slot_array;
extern unsigned long *working_flag_array;
extern unsigned long *progress_array;

extern char *client_flag;
extern char *server_flag_array;

extern Semaphore *sem_1;
extern Semaphore *sem_array;
extern Semaphore *sem_working_array;



//--------------------------------------------//
//       Method Declarations(Prototypes)      //
//--------------------------------------------//
void trial_division_sqrt(unsigned long n, int slot_num);
void print_as_binary(unsigned long n);
void factor_task(void *arg);
void test_task(void *arg);




//--------------------------------------------//
//           Initialize Threadpool            //
//--------------------------------------------//
void server_init(int number_of_threads)
{
	thpool = thpool_init(number_of_threads);
}



//--------------------------------------------//
//               Server Main Loop             //
//--------------------------------------------//
void run_server()
{
	int i = 0;
	for (; i < 10; ++i)
	{
		make_semaphore(&progress_sem[i], 1);
	}

	unsigned long num = 0;
	

	while(1)
	{
		int slot_num;

		while(*client_flag != '1')
		{
			usleep(100);
		}

		int i = 0;
		for (; i < 10; ++i)
		{
			if(working_flag_array[i] == 0)
			{
				slot_num = i;
				
				sem_wait(&sem_working_array[i]);
				working_flag_array[i] = number_of_threads;
				sem_signal(&sem_working_array[i]);

				break;
			}
		}
		
		sem_wait(sem_1);
		num = *number;
		*number = slot_num;
		*client_flag = '0';
		sem_signal(sem_1);


		if(num == 0)
		{
			server_testQuery();
		}
		else
		{
			server_query(num, slot_num); 
		}

	}
}




//--------------------------------------------//
//      Standard Query Handling Methods       //
//--------------------------------------------//
void server_query(unsigned long num, int slot_num)
{
    sem_wait(&progress_sem[slot_num]);
    progress_array[slot_num] = number_of_threads * 100;
    sem_signal(&progress_sem[slot_num]);
	

	Job *job_ptr;

	int i = 0;
	for (; i < number_of_threads; i++)
	{
		Job job;
		job_ptr = &job;

		unsigned long c = num << (32 - i);
		unsigned long k = (num >> i) | c;

		job_ptr->data = k;
		job_ptr->query_number = slot_num;

		thpool_add_work(thpool, (void*)factor_task, job_ptr);

		usleep(100);
		//printf("Shift %d : %u\n", i, k);
		//print_as_binary(k);
	}


}


void factor_task(void *arg)
{
	unsigned long num = 0;

	Job *job = arg;
	

	if(job == NULL)
	{
		perror("job == null | factor_task");
		return;
	}

	num = job->data;


	trial_division_sqrt(num, job->query_number);

}


void trial_division_sqrt(unsigned long n, int slot_num) 
{
	int percent_array[20];
	
	int i = 0;
	for(; i < 20; i++) percent_array[i] = 0;

	double calced_percent = 0;

    unsigned long sqrt_n = (unsigned long)sqrt((double)n);
    
    unsigned long x = 2;

    for(; x < sqrt_n; x++) 
    {
        if ((n % x) == 0) 
        {
        	while (server_flag_array[slot_num] != '0')
			{
				usleep(100);
			}
			
			sem_wait(&sem_array[slot_num]);
			server_flag_array[slot_num] = '1';
			slot_array[slot_num] = x;
			sem_signal(&sem_array[slot_num]); 
        }

        calced_percent = (((double)x)/sqrt_n)*100;//(x / sqrt_n) * 100;
        int num_of_fives = (int)calced_percent / 5;

        i = 0;
        for(; i < num_of_fives; i++)
        {
        	if(percent_array[i] == 0)
        	{
        		percent_array[i] = 1;

			    sem_wait(&progress_sem[slot_num]);
			    progress_array[slot_num] -= 5;
			    sem_signal(&progress_sem[slot_num]);
        	}
        }

    }


    i = 0;
    for(; i < 20; i++)
    {
    	if(percent_array[i] == 0)
    	{
    		percent_array[i] = 1;

		    sem_wait(&progress_sem[slot_num]);
		    progress_array[slot_num] -= 5;
		    sem_signal(&progress_sem[slot_num]);
    	}
    }

    sem_wait(&sem_working_array[slot_num]);
	working_flag_array[slot_num]--;
	sem_signal(&sem_working_array[slot_num]);
}






//--------------------------------------------//
//         Test Query Handling Methods        //
//--------------------------------------------//
void server_testQuery()
{
	working_flag_array[0] = 10;
	working_flag_array[1] = 10;
	working_flag_array[2] = 10;

	int i = 0;
	int arg = 0;

	Job job1, job2, job3;
	
	Job *job_ptr = &job1;
	job_ptr->query_number = 0;
	for (; i < 10; i++)
	{
		job_ptr->data = i;
		
		thpool_add_work(thpool, (void*)test_task, job_ptr);
		usleep(50000);
	}

	job_ptr = &job2;
	job_ptr->query_number = 1;
	for (i = 0; i < 10; i++)
	{
		job_ptr->data = 10 + i;
		thpool_add_work(thpool, (void*)test_task, job_ptr);
		usleep(50000);
	}

	job_ptr = &job3;
	job_ptr->query_number = 2;
	for (i = 0; i < 10; i++)
	{
		job_ptr->data = 20 + i;
		thpool_add_work(thpool, (void*)test_task, job_ptr);
		usleep(50000);
	}

}


void test_task(void *arg)
{
	Job *job = arg;

	int num = 0;
	
	if(job != NULL)
	{
		num = job->data;
	}

	//printf("Thread #%u working on %d..... %d\n", (int)pthread_self(), num, job->query_number);

	while (server_flag_array[job->query_number] != '0')
	{
		usleep(100);
	}
	
	sem_wait(&sem_array[job->query_number]);
	server_flag_array[job->query_number] = '1';
	slot_array[job->query_number] = num;
	sem_signal(&sem_array[job->query_number]);

	sem_wait(&sem_working_array[job->query_number]);
	working_flag_array[job->query_number]--;
	sem_signal(&sem_working_array[job->query_number]);
}




//--------------------------------------------//
//               Shutdown Method              //
//--------------------------------------------//
void server_shutdown()
{
	thpool_destroy(thpool);
}



//--------------------------------------------//
//          Used For Testing Shifting         //
//--------------------------------------------//
void print_as_binary(unsigned long n)
{
	int k, c;

	printf("%u in binary number system is:\n", n);
 
	for (c = 31; c >= 0; c--)
	{
		k = n >> c;

		if (k & 1)
		  printf("1");
		else
		  printf("0");
	}

	printf("\n\n");
}


