//--------------------------------------------------//
//                                                  //
//    Assinment 2 - Multi-Threaded Client Server    //
//                                                  //
//            David Farrow, s2917529                //  
//      Griffith University, Gold Coast, 2015       // 
//                                                  //
//                server.h | Server                 //
//                                                  //
//--------------------------------------------------//

#ifndef _SERVER_
#define _SERVER_

#include "threadpool.h" 	/* MIT thread pool */
#include "globals.h"


//--------------------------------------------//
//                 Job Struct                 //
//--------------------------------------------//
typedef struct
{
	int query_number;
	unsigned long data;
}Job;


//--------------------------------------------//
//              Global variables              //
//--------------------------------------------//
threadpool thpool;


//--------------------------------------------//
//                   Methods                  //
//--------------------------------------------//
//-- Sets the number of threads in thread pool 
void server_init(int number_of_threads);

//-- Server main loop
void run_server();

//-- Standard query
void server_query(unsigned long num, int slot_num);

//-- '0' test query
void server_testQuery();

//-- Destroy treadpool
void server_shutdown();

#endif
