//--------------------------------------------------//
//                                                  //
//    Assinment 2 - Multi-Threaded Client Server    //
//                                                  //
//            David Farrow, s2917529                //  
//      Griffith University, Gold Coast, 2015       // 
//                                                  //
//             semaphore.h | Semaphore              //
//                                                  //
//--------------------------------------------------//

#ifndef SEMAPHORE_
#define SEMAPHORE_

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>



//--------------------------------------------//
//              Semaphore Struct              //
//--------------------------------------------//
typedef struct 
{
	int value; 				//- the semaphore’s value
	int wakeups; 			//- the number of pending signals to avoid thread starvation
	pthread_mutex_t lock; 	//- used to protect value and wakeups
	pthread_cond_t cond;	//- for waiting on the semaphore
} Semaphore;
 

//--------------------------------------------//
//                   Methods                  //
//--------------------------------------------//
//-- Create/initialize Semaphore
void make_semaphore(Semaphore* sem, int value);

//-- Destroy Semaphore
void destroy_semaphore(Semaphore* sem);

//-- Wait for signal
void sem_wait(Semaphore* sem);

//-- Signal waiting threads 
void sem_signal(Semaphore* sem);

#endif
