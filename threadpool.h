//--------------------------------------------------//
//                                                  //
//    Assinment 2 - Multi-Threaded Client Server    //
//                                                  //
//            David Farrow, s2917529                //  
//      Griffith University, Gold Coast, 2015       // 
//                                                  //
//            threadpool.h | Threadpool             //
//           										//
//--------------------------------------------------//

#ifndef _THPOOL_
#define _THPOOL_

//--------------------------------------------//
//             Threadpool Struct              //
//--------------------------------------------//
typedef struct thpool_* threadpool;


//--------------------------------------------//
//                   Methods                  //
//--------------------------------------------//
//-- Initialize threadpool
threadpool thpool_init(int num_threads);

//-- Add a job to the threadpool
int thpool_add_work(threadpool, void *(*function_p)(void*), void* arg_p);

//-- Wait for all threadpool jobs to finish
void thpool_wait(threadpool);

//-- Pause threadpool
void thpool_pause(threadpool);

//-- Unpause / resume threadpool
void thpool_resume(threadpool);

//-- Destroy threadpool
void thpool_destroy(threadpool);

#endif
