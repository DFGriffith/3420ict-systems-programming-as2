//--------------------------------------------------//
//                                                  //
//    Assinment 2 - Multi-Threaded Client Server    //
//                                                  //
//            David Farrow, s2917529                //  
//      Griffith University, Gold Coast, 2015       // 
//                                                  //
//             semaphore.c | Semaphore              //
//                                                  //
//--------------------------------------------------//

#include "semaphore.h"


//--------------------------------------------//
//            Initialize Semaphore            //
//--------------------------------------------//
void make_semaphore(Semaphore* sem, int value) 
{
	sem->value = value;
	sem->wakeups = 0;
	
    pthread_mutex_init(&sem->lock, NULL);
    pthread_cond_init(&sem->cond, NULL);
}


//--------------------------------------------//
//              Destroy Semaphore             //
//--------------------------------------------//
void destroy_semaphore(Semaphore* sem)
{
	pthread_mutex_destroy(&sem->lock);
    pthread_cond_destroy(&sem->cond);
}


//--------------------------------------------//
//               Wait For Signal              //
//--------------------------------------------//
void sem_wait(Semaphore* sem)
{
    pthread_mutex_lock(&sem->lock);

    while(sem->value <= 0)
    {
        pthread_cond_wait(&sem->cond, &sem->lock);
    }

    sem->value--;

    pthread_mutex_unlock(&sem->lock);
} 


//--------------------------------------------//
//          Signal Waiting Semaphores         //
//--------------------------------------------//
void sem_signal(Semaphore* sem)
{
    pthread_mutex_lock(&sem->lock);

    sem->value++;

    pthread_cond_signal(&sem->cond);
    pthread_mutex_unlock(&sem->lock);
}